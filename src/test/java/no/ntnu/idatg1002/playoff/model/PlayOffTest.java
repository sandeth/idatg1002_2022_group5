package no.ntnu.idatg1002.playoff.model;

import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
/**
 * A test class to test the functionality of PlayOff class.
 * @author Eimen Oueslati
 * @version 1.0 - SNAPSHOT
 */
class PlayOffTest {
    private ArrayList<String> players;
    private ArrayList<Team> qualified;
    private PlayOff playoff;

    /**
     * this method initialises the class fields.
     * it initialises abd populate the qualified teams arraylist, PlayOff object and the players arrayList.
     */
    @BeforeEach
    void setUp() {
        players = new ArrayList<>();
        qualified = new ArrayList<>();
        players.addAll(Arrays.asList("Andreas", "Sander", "Nicolai", "Malin", "Eimen"));
        for(int i = 0; i < 4; i++)
        {
            qualified.add(new Team("team " + i, players));
        }
        playoff = new PlayOff(qualified,true);
    }

    /**
     * this methode test the PlayOff class constructor. it tests if the objects are
     * created properly and if the exceptions are handeled as they should be.
     */
    @Test
    void constructor() {
        assertEquals(4, playoff.getBrackets().size());
        List<Team> failList = new ArrayList<>();
        for(Team team : qualified){
            failList.add(team);
        }
        failList.remove(3);
        failList.add(failList.get(2));
        try {
            new PlayOff(failList,true);
        }
        catch(IllegalArgumentException e)
        {
            assertEquals("the list of teams can not have duplicates", e.getMessage());
        }


        for(int i = 0; i < 4; i++)
        {
            qualified.add(new Team("dummy team " + i, players));
            if(i == 3)
            {
                assertEquals(8, new PlayOff(qualified,false).getBrackets().size());
            }
            else
            {
                //tests if the constructors allows only the correct arraylist sizes
                try {
                    new PlayOff(qualified,false);
                }
                catch (IllegalArgumentException e)
                {
                    assertEquals("the qualified teams list must be of adequate size", e.getMessage());
                }
            }
        }

    }

    /**
     * this method tests the functionality of the updateBrackets method
     * it tests that the method works as it should
     */
    @Test
    void updateBrackets() {
        // At first there are 4 teams in the playoff
        assertEquals(4,playoff.getBrackets().size());
        // Setting score in all matches, to make one team the winner
        playoff.getCurrentMatches().forEach(match -> {match.setScore(3,2);});
        // Updating the bracket system. Losing team should be removed.
        playoff.updateBrackets();
        // Expecting there to only be 2 teams left
        assertEquals(2 ,playoff.getBrackets().size());
        // Setting score in matches
        playoff.getCurrentMatches().forEach(match -> {match.setScore(3,2);});
        // Updating bracket system
        playoff.updateBrackets();
        // Expecting there to only be one team left
        assertEquals(1,playoff.getBrackets().size());


    }

    @Test
    void getWinner() {

        // Trying to call getWinner when there are several teams left
        // Expecting an exception to be thrown
        try{
            playoff.getWinner();
        }catch (IllegalStateException e){
            assertEquals("the tournament has not ended yet",e.getMessage());
        }
        // Will try to make the first team in the list, the winner
        Team expectedToWin = playoff.getBrackets().get(0);
        // Currently, 4 teams in the bracket
        assertEquals(4,playoff.getBrackets().size());
        // Setting score and updating bracket until only one team is left
        playoff.getCurrentMatches().forEach(match -> match.setScore(1,0));
        playoff.updateBrackets();
        assertEquals(2,playoff.getBrackets().size());
        playoff.getCurrentMatches().forEach(match -> match.setScore(1,0));
        playoff.updateBrackets();
        // Using the getWinner method to get the single team left
        try {
            Team winner = playoff.getWinner();
            // Expecting winner to not be null
            assertNotNull(winner);
            assertEquals(playoff.getBrackets().get(0),winner);
            assertEquals(expectedToWin,winner);
        }catch (IllegalStateException e){
            fail();
        }

    }

    @Test
    void getBrackets() {
        List<Team> firstTeamList = playoff.getBrackets();
        List<Team> secondTeamList = playoff.getBrackets();

        // firstTeamList and secondTeamList should be the same
        assertEquals(firstTeamList,secondTeamList);

        // The size of both of the list should be 4
        assertEquals(4,firstTeamList.size());
        // Setting score and updating bracket
        playoff.getCurrentMatches().forEach(match -> match.setScore(1,0));
        playoff.updateBrackets();
        // Size of the list returned with the getBrackets method is expected to be 2
        assertEquals(2,playoff.getBrackets().size());

    }

}