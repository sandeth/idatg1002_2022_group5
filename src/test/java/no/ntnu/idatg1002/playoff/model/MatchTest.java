package no.ntnu.idatg1002.playoff.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class MatchTest {
  Team team1;
  Team team2;
  Match match;

  @BeforeEach
  public void constructor() {
    team1 = new Team("Blue");
    team2 = new Team("Red");
    match = new Match(team1, team2);
  }

  @Test
  @DisplayName("Testing setScore Method")
  void setScore() {
    match.setScore(10, 11);
    assertEquals(10, match.getTeamOneScore());
    assertEquals(11, match.getTeamTwoScore());

    IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () ->
        match.setScore(0,-1), "Score needs to be higher than 0.");
  }

  @Test
  @DisplayName("Testing method getWinner")
  void getWinner() {
    match.setScore(0,0);
    assertNull(match.getWinner());
    match.setScore(1,0);
    assertEquals(match.getWinner(), team1);
    match.setScore(0,1);
    assertEquals(match.getWinner(), team2);
  }

  @Test
  @DisplayName("Testing method getLoser")
  void getLoser() {
    match.setScore(0,0);
    assertNull(match.getLoser());
    match.setScore(1,0);
    assertEquals(match.getLoser(), team2);
    match.setScore(0,1);
    assertEquals(match.getLoser(), team1);
  }

}