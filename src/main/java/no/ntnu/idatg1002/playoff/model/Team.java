package no.ntnu.idatg1002.playoff.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Class to represent a team participating
 * in a tournament.
 *
 * @author Andreas Follevaag Malde
 * @version 1.0 - SNAPSHOT
 */
public class Team implements Comparable<Team> {

  private String name;
  private List<String> players;
  private List<Team> teamsPlayed;
  private int groupPoints;

  /**
   * Constructor to initialize a new team
   * with no players. Only a team name.
   *
   * @param teamName Name of the team
   */
  public Team(String teamName) {
    this.setTeamName(teamName);
    this.players = new ArrayList<>();
    this.teamsPlayed = new ArrayList<>();
    this.groupPoints = 0;
  }

  /**
   * Constructor to initialize a team with players
   * added from the start.
   *
   * @param teamName Name of the team
   * @param players  List of players on the team
   */
  public Team(String teamName, List<String> players) {
    this.setTeamName(teamName);
    if (players == null) {
      throw new IllegalArgumentException("Players list can not be null.");
    }
    this.players = players;
    this.teamsPlayed = new ArrayList<>();
    this.groupPoints = 0;
  }

  /**
   * Get the team name.
   *
   * @return Name of the team
   */
  public String getName() {
    return name;
  }

  /**
   * Set the team name. Important to make sure that different
   * teams in a tournament have different names.
   *
   * @param teamName Name of the team.
   */
  public void setTeamName(String teamName) {
    if (teamName.isBlank() || teamName.isEmpty()) {
      throw new IllegalArgumentException("Name can not be blank or empty");
    }
    this.name = teamName;
  }

  /**
   * Get the players of the team.
   *
   * @return List of team players
   */
  public List<String> getPlayers() {
    return players;
  }

  /**
   * Get an overview of teams the team has
   * played against during the tournament.
   *
   * @return List of teams played against
   */
  public List<Team> getTeamsPlayed() {
    return teamsPlayed;
  }

  /**
   * Method for returning a groups points.
   *
   * @return group points
   */
  public int getGroupPoints() {
    return groupPoints;
  }

  /**
   * Add a team to the team played list to keep
   * track of which teams this team has played against.
   * Will throw exceptions if team is already in the list, or
   * if the parameter is null or this object
   *
   * @param team Team to add to the teams played list
   */
  public void addTeamToPlayed(Team team) {
    if (teamsPlayed.contains(team) || this.equals(team) || team == null) {
      throw new IllegalArgumentException(
          "Operation failed... Check if team is already in the list.");
    }
    teamsPlayed.add(team);
  }

  /**
   * Add group points to the team after a match.
   * Points have to be between 0 and 3.
   *
   * @param points Win = 3 point, Draw = 1 point
   */
  public void addGroupPoints(int points) {
    if (points > 3 || points < 0) {
      throw new IllegalArgumentException("Points have to be between 0 and 3.");
    }
    this.groupPoints += points;
  }

  /**
   * Overriding default equals method. The equals' method
   * will compare objects based on the team name. Different
   * teams need to have different team names.
   *
   * @param o Object to compare against
   * @return true / false
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Team team = (Team) o;
    return Objects.equals(name, team.name);
  }

  /**
   * Overriding the default hashcode method. A hashcode
   * will be made based on the team name.
   *
   * @return Hashode of the team object
   */
  @Override
  public int hashCode() {
    return Objects.hash(name);
  }

  /**
   * override the compare to method from the Comparable interface.
   *
   * @param compareTo shows the team with the highest value.
   * @return who has the most points
   */
  @Override
  public int compareTo(Team compareTo) {
    return compareTo.getGroupPoints() - this.getGroupPoints();
  }
}
