package no.ntnu.idatg1002.playoff.model;

/**
 * Class to represent the match
 * between two teams.
 *
 * @author Sander Hauge, Andreas Malde
 * @version 1.0 - SNAPSHOT
 */
public class Match {
  private Team team1;
  private Team team2;
  private String representation;

  private int teamOneScore;
  private int teamTwoScore;

  /**
   * Constructor for match.
   *
   *  @param team1 First match team.
   * @param team2 Second match team.
   */
  public Match(Team team1, Team team2) {
    if (team1 == null || team2 == null || team1.equals(team2)) {
      throw new IllegalArgumentException("The teams can not be null or the same team.");
    }

    this.team1 = team1;
    this.team2 = team2;
    teamOneScore = 0;
    teamTwoScore = 0;
    this.representation = this.team1.getName() + " vs. " + this.team2.getName();
  }

  /**
   * Method to return the winning team of a match.
   *
   *  @return Winning team of match. Returns null if its draw.
   */
  public Team getWinner() {
    if (teamOneScore > teamTwoScore) {
      return team1;
    } else if (teamTwoScore > teamOneScore) {
      return team2;
    }
    return null;
  }

  /**
   * Method to return the losing team of a match.
   *
   * @return Losing team of match. Returns null if its draw.
   */
  public Team getLoser() {
    if (teamOneScore < teamTwoScore) {
      return team1;
    } else if (teamTwoScore < teamOneScore) {
      return team2;
    }
    return null;
  }

  public String getRepresentation() {
    return this.representation;
  }

  /**
   * Setter for the score of the match.
   *
   * @param teamOneScore Integer for the score of team one.
   * @param teamTwoScore Integer for the score of team two.
   */
  public void setScore(int teamOneScore, int teamTwoScore) throws IllegalArgumentException {
    if (teamOneScore < 0 || teamTwoScore < 0) {
      throw new IllegalArgumentException("Score needs to be higher than 0.");
    } else {
      this.teamOneScore = teamOneScore;
      this.teamTwoScore = teamTwoScore;
    }
  }

  /**
   * Method for returning team one.
   *
   * @return team one
   */
  public Team getTeamOne() {
    return team1;
  }

  /**
   * Method for returning team two.
   *
   * @return return team two.
   */
  public Team getTeamTwo() {
    return team2;
  }

  /**
   * Method for returning  team one's score.
   *
   * @return team one score
   */
  public int getTeamOneScore() {
    return teamOneScore;
  }

  /**
   * Method for returning  team two's score.
   *
   * @return team two score
   */
  public int getTeamTwoScore() {
    return teamTwoScore;
  }



  /**
   * Present the match as "Team1 vs. Team2" for
   * a more visual presentation.
   *
   * @return "Team1 vs. Team2"
   */
  @Override
  public String toString() {
    return team1.getName() + " vs. " + team2.getName();
  }

}
