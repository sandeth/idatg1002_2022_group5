package no.ntnu.idatg1002.playoff.controllers;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import no.ntnu.idatg1002.playoff.Main;
import no.ntnu.idatg1002.playoff.model.utility.FileHandler;

/**
 * Controller class to connect the start screen
 * GUI to the backend model.
 *
 * @author Andreas Follevaag Malde, Sander Hauge
 * @version 1.0 - SNAPSHOT
 */
public class MainMenuController implements Initializable {

  @FXML private ImageView previousTournamentLogo;
  @FXML private ImageView newTournamentLogo;
  @FXML private ImageView logo;

  @FXML
  private void onExitClicked() {
    Main.exitConfirmation((Stage) logo.getScene().getWindow());
  }

  /**
   * Event handler for, New tournament button.
   *
   * @throws IOException If the button gets a faulty input.
   */
  @FXML
  private void onNewTournamentCreateClicked() throws IOException {
    TextInputDialog nameDialog = new TextInputDialog();
    nameDialog.setHeaderText("Enter a tournament name:");
    Optional<String> name = nameDialog.showAndWait();

    if (name.isPresent()) {
      String nameGet = name.get();
      if ((nameGet.isEmpty() || nameGet.isBlank())) {
        new Alert(Alert.AlertType.ERROR,
            "A tournament with no name can not be created.").showAndWait();
      } else {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("tournament-mainpage.fxml"));
        Stage stage = (Stage) logo.getScene().getWindow();
        Scene scene = new Scene(fxmlLoader.load(), 800, 600);
        TournamentController controller = fxmlLoader.getController();
        controller.newTournament(nameGet);
        stage.setScene(scene);
      }
    }
  }

  /**
   * Action handler for button press on about tap in help menu.
   */
  @FXML
  private void onAboutButtonPressed() {
    Alert alert = new Alert(Alert.AlertType.INFORMATION, "About");
    alert.setContentText(
        "This is an application made by Sander, Eimen, Andreas, Malin\nand Nicolai!");
    alert.setTitle("About");
    alert.setHeaderText("This is a tournament manager for Football.");
    alert.showAndWait();
  }

  /**
   * Constructor for javaFX classes.
   */
  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {
    logo.setImage(new Image(String.valueOf(
        MainMenuController.class.getResource("PlayOffLogo.png"))));
    previousTournamentLogo.setImage(new Image(String.valueOf(MainMenuController.class.getResource("import.png"))));
    newTournamentLogo.setImage(new Image(String.valueOf(MainMenuController.class.getResource("file-plus.png"))));
  }

  /**
   * Method to open file handler window to find a saved tournament.
   */
  @FXML
  private void onPreviousTournamentsPressed() {
    FileChooser chooser = new FileChooser();

    try {
      Path selectedFile =
          FileHandler.getFilePath(chooser.showOpenDialog(logo.getScene().getWindow()));
      FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("tournament-mainpage.fxml"));
      Stage stage = (Stage) logo.getScene().getWindow();
      Scene scene = new Scene(fxmlLoader.load(), 800, 600);
      TournamentController controller = fxmlLoader.getController();
      controller.openFromFile(FileHandler.readFromFile(selectedFile));
      stage.setScene(scene);
    } catch (Exception e) {
      Alert alert = new Alert(Alert.AlertType.WARNING, "File not found.");
      alert.showAndWait();
    }
  }
}