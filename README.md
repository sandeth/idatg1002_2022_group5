PLAYOFF - A TOURNAMENT MANAGER
------------------------------

This is a project for the class Software Engineering at NTNU, group 5.

##### Developers
* Andreas Follevaag Malde
* Nicolai Olsen
* Sander Tøkje Hauge
* Malin Sofie Grytan
* Eimen Oueslati

##### Introduction
PlayOff is an application for managing tournaments. It's focus was mainly
on managing football tournaments, but it also works for other sport tournaments
like handball, basketball etc. 

##### Compatibility and system requirements
The program currently only supports Windows as operating system. There are no
support for macOS and Linux yet. This is a stand-alone application meaning there
is no need to install other programs for it to work. Most modern-day Windows computers will have no problem running this application. There is no minimum requirements, except 300 MB free disk space on your computer for the application and the following folders. However, for a smoother experience, it is recommended with at least 4 GB of RAM. 

##### How to install and start the application
In this folder there is a zip file named PlayOff.zip. After extracting this file, you are presented with a folder containing a resource folder, PlayOff.exe, a readme/installation guide and a user manual.
Just double-click the .exe file and the application will launch. No installation of java or anything else needed.
It is important to let all files in the playoff folder stay in their locations. Trying to move the PlayOff.exe out of the PlayOff folder will cause the application to not launch. One can make a shortcut to the application by right-clicking the application and selecting "Make shortcut". This shortcut can be placed anywhere on the computer. Double-clicking this shortcut will launch the application.

##### Javadoc 
The javadoc of all source files in this project can be found in the javadoc folder in this project. From there navigate to the index.html file. Open the file in the web browser of your choice.
The javadoc is also available on this URL: https://folk.ntnu.no/andrefma/

